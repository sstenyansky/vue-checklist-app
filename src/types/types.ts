export type TaskStatus = 'active' | 'completed';

export type TaskType = {
  name: string
  text: string
  status: TaskStatus
  id: string
};

export type TaskData = Partial<TaskType>;

export type AddTaskData = Pick<TaskType, "name" | "text">;