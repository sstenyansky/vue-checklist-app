import { customAlphabet } from "nanoid";

const nanoid = customAlphabet('abcdefghiklmnopqrstuvxyz1234567890', 5);

export default nanoid